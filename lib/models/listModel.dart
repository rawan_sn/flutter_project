import 'dart:convert';

listFromApi advertisingFromJson(String str) => listFromApi.fromJson(json.decode(str));

String advertisingToJson(listFromApi data) => json.encode(data.toJson());

class listFromApi {
  listFromApi({
    this.code,
    this.status,
    this.data,
    this.message,
  });

  int code;
  String status;
  Data data;
  String message;

  factory listFromApi.fromJson(Map<String, dynamic> json) => listFromApi(
    code: json["code"],
    status: json["status"],
    data: Data.fromJson(json["data"]),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "status": status,
    "data": data.toJson(),
    "message": message,
  };
}

class Data {
  Data({
    this.items,
    this.pagination,
    this.statement,
  });

  List<Item> items;
  Pagination pagination;
  dynamic statement;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
    pagination: Pagination.fromJson(json["pagination"]),
    statement: json["statement"],
  );

  Map<String, dynamic> toJson() => {
    "items": List<dynamic>.from(items.map((x) => x.toJson())),
    "pagination": pagination.toJson(),
    "statement": statement,
  };
}

class Offer {
  Offer({
    this.id,
    this.adId,
    this.comment,
    this.private,
    this.price,
    this.quantity,
    this.state,
    this.createdAt,
    this.updatedAt,
    this.trackingReference,
    this.linkedAdId,
    this.deliveryDate,
    this.deliveryFees,
    this.acceptedByBuyer,
    this.ad,
    this.user,
  });

  int id;
  int adId;
  String comment;
  bool private;
  String price;
  String quantity;
  String state;
  DateTime createdAt;
  DateTime updatedAt;
  String trackingReference;
  dynamic linkedAdId;
  dynamic deliveryDate;
  dynamic deliveryFees;
  dynamic acceptedByBuyer;
  Item ad;
  User user;

  factory Offer.fromJson(Map<String, dynamic> json) => Offer(
    id: json["id"],
    adId: json["ad_id"],
    comment: json["comment"],
    private: json["private"],
    price: json["price"],
    quantity: json["quantity"],
    state: json["state"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    trackingReference: json["tracking_reference"],
    linkedAdId: json["linked_ad_id"],
    deliveryDate: json["delivery_date"],
    deliveryFees: json["delivery_fees"],
    acceptedByBuyer: json["accepted_by_buyer"],
    ad: Item.fromJson(json["ad"]),
    user: User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "ad_id": adId,
    "comment": comment,
    "private": private,
    "price": price,
    "quantity": quantity,
    "state": state,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "tracking_reference": trackingReference,
    "linked_ad_id": linkedAdId,
    "delivery_date": deliveryDate,
    "delivery_fees": deliveryFees,
    "accepted_by_buyer": acceptedByBuyer,
    "ad": ad.toJson(),
    "user": user.toJson(),
  };
}

class Item {
  Item({
    this.id,
    this.userId,
    this.marketerId,
    this.title,
    this.state,
    this.views,
    this.price,
    this.paymentMethods,
    this.cityId,
    this.sponsored,
    this.trackingReference,
    this.createdAt,
    this.published,
    this.minOrderQuantity,
    this.unitWeight,
    this.unitPrice,
    this.quantity,
    this.sold,
    this.productionDate,
    this.plateNumber,
    this.isPurchaseRequest,
    this.isStopped,
    this.displayPrice,
    this.favorite,
    this.isNew,
    this.userAdsCount,
    this.imageUrl,
    this.cityName,
    this.category,
    this.market,
    this.user,
    this.marketer,
    this.productionCompany,
    this.productName,
    this.details,
    this.contactPhone,
    this.pricingType,
    this.lat,
    this.long,
    this.addressLine1,
    this.paymentMethod,
    this.isBlocked,
    this.videosUrls,
    this.voicesUrls,
    this.adUnitType,
    this.images,
    this.sounds,
    this.comments,
    this.offers,
  });

  int id;
  int userId;
  dynamic marketerId;
  String title;
  String state;
  int views;
  String price;
  String paymentMethods;
  int cityId;
  bool sponsored;
  String trackingReference;
  DateTime createdAt;
  bool published;
  int minOrderQuantity;
  double unitWeight;
  double unitPrice;
  double quantity;
  String sold;
  int productionDate;
  dynamic plateNumber;
  bool isPurchaseRequest;
  dynamic isStopped;
  String displayPrice;
  bool favorite;
  bool isNew;
  int userAdsCount;
  String imageUrl;
  String cityName;
  Category category;
  Market market;
  // Marketer user; //User
  User user;
  Marketer marketer;
  dynamic productionCompany;
  dynamic productName;
  String details;
  String contactPhone;
  String pricingType;
  dynamic lat;
  dynamic long;
  dynamic addressLine1;
  dynamic paymentMethod;
  dynamic isBlocked;
  List<dynamic> videosUrls;
  List<dynamic> voicesUrls;
  dynamic adUnitType;
  List<Image> images;
  List<Sound> sounds;
  List<Comment> comments;
  List<Offer> offers;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
    id: json["id"],
    userId: json["user_id"],
    marketerId: json["marketer_id"],
    title: json["title"],
    state: json["state"],
    views: json["views"],
    price: json["price"],
    paymentMethods: json["payment_methods"],
    cityId: json["city_id"],
    sponsored: json["sponsored"],
    trackingReference: json["tracking_reference"],
    createdAt: DateTime.parse(json["created_at"]),
    published: json["published"],
    minOrderQuantity: json["min_order_quantity"],
    unitWeight: json["unit_weight"],
    unitPrice: json["unit_price"],
    quantity: json["quantity"],
    sold: json["sold"],
    productionDate: json["production_date"],
    plateNumber: json["plate_number"],
    isPurchaseRequest: json["is_purchase_request"],
    isStopped: json["is_stopped"],
    displayPrice: json["display_price"],
    favorite: json["favorite"],
    isNew: json["is_new"],
    userAdsCount: json["user_ads_count"],
    imageUrl: json["image_url"],
    cityName: json["city_name"],
    category: Category.fromJson(json["category"]),
    market: json["market"] == null ? null : Market.fromJson(json["market"]),
    // user: Marketer.fromJson(json["user"]),
    user:User.fromJson(json["user"]),
    marketer:json["marketer"]==null?null: Marketer.fromJson(json["marketer"]),
    productionCompany: json["production_company"],
    productName: json["product_name"],
    details: json["details"],
    contactPhone: json["contact_phone"],
    pricingType: json["pricing_type"],
    lat: json["lat"],
    long: json["long"],
    addressLine1: json["address_line1"],
    paymentMethod: json["payment_method"],
    isBlocked: json["is_blocked"],
    videosUrls: List<dynamic>.from(json["videos_urls"].map((x) => x)),
    voicesUrls: List<dynamic>.from(json["voices_urls"].map((x) => x)),
    adUnitType: json["ad_unit_type"],
    images: json["images"] == null ? null :List<Image>.from(json["images"].map((x) => Image.fromJson(x))),
    sounds: json["sounds"] == null ? null :List<Sound>.from(json["sounds"].map((x) => Sound.fromJson(x))),
    comments:  json["comments"] == null ? null :List<Comment>.from(json["comments"].map((x) => Comment.fromJson(x))),
    offers: json["offers"] == null ? null : List<Offer>.from(json["offers"].map((x) => Offer.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "marketer_id": marketerId,
    "title": title,
    "state": state,
    "views": views,
    "price": price,
    "payment_methods": paymentMethods,
    "city_id": cityId,
    "sponsored": sponsored,
    "tracking_reference": trackingReference,
    "created_at": createdAt.toIso8601String(),
    "published": published,
    "min_order_quantity": minOrderQuantity,
    "unit_weight": unitWeight,
    "unit_price": unitPrice,
    "quantity": quantity,
    "sold": sold,
    "production_date": productionDate,
    "plate_number": plateNumber,
    "is_purchase_request": isPurchaseRequest,
    "is_stopped": isStopped,
    "display_price": displayPrice,
    "favorite": favorite,
    "is_new": isNew,
    "user_ads_count": userAdsCount,
    "image_url": imageUrl,
    "city_name": cityName,
    "category": category.toJson(),
    "market": market == null ? null : market.toJson(),
    "user": user.toJson(),
    "marketer": marketer==null?null:marketer.toJson(),
    "production_company": productionCompany,
    "product_name": productName,
    "details": details,
    "contact_phone": contactPhone,
    "pricing_type": pricingType,
    "lat": lat,
    "long": long,
    "address_line1": addressLine1,
    "payment_method": paymentMethod,
    "is_blocked": isBlocked,
    "videos_urls": List<dynamic>.from(videosUrls.map((x) => x)),
    "voices_urls": List<dynamic>.from(voicesUrls.map((x) => x)),
    "ad_unit_type": adUnitType,
    "images": images == null ? null :List<dynamic>.from(images.map((x) => x.toJson())),
    "sounds": sounds == null ? null :List<dynamic>.from(sounds.map((x) => x.toJson())),
    "comments": comments == null ? null :List<dynamic>.from(comments.map((x) => x.toJson())),
    "offers": offers == null ? null : List<dynamic>.from(offers.map((x) => x.toJson())),
  };
}

class Marketer {
  Marketer({
    this.id,
    this.phone,
    this.authorized,
    this.name,
    this.imageUrl,
    this.shoppingCountryId,
    this.email,
    this.title,
    this.accessCentralMarket,
    this.accessFruitsMarket,
    this.accessDatesMarket,
    this.accessEquipmentsMarket,
    this.accessUtilitiesMarket,
    this.accessServicesMarket,
    this.accessOccasions,
    this.isPrivate,
    this.isMarketer,
    this.accountType,
    this.isAdmin,
    this.description,
    this.masterUserId,
    this.subUserCount,
    this.subUserRequestCount,
    this.rating,
    this.type,
    this.linkType,
    this.isFollowed,
    this.deviceTokens,
  });

  int id;
  String phone;
  bool authorized;
  String name;
  String imageUrl;
  int shoppingCountryId;
  String email;
  dynamic title;
  bool accessCentralMarket;
  bool accessFruitsMarket;
  bool accessDatesMarket;
  bool accessEquipmentsMarket;
  bool accessUtilitiesMarket;
  bool accessServicesMarket;
  bool accessOccasions;
  bool isPrivate;
  bool isMarketer;
  String accountType;
  bool isAdmin;
  String description;
  int masterUserId;
  int subUserCount;
  int subUserRequestCount;
  double rating;
  String type;
  LinkType linkType;
  bool isFollowed;
  List<DeviceToken> deviceTokens;

  factory Marketer.fromJson(Map<String, dynamic> json) => Marketer(
    id: json["id"],
    phone: json["phone"],
    authorized: json["authorized"],
    name: json["name"],
    imageUrl: json["image_url"] == null ? null : json["image_url"],
    shoppingCountryId: json["shopping_country_id"],
    email: json["email"],
    title: json["title"],
    accessCentralMarket: json["access_central_market"],
    accessFruitsMarket: json["access_fruits_market"],
    accessDatesMarket: json["access_dates_market"],
    accessEquipmentsMarket: json["access_equipments_market"],
    accessUtilitiesMarket: json["access_utilities_market"],
    accessServicesMarket: json["access_services_market"],
    accessOccasions: json["access_occasions"],
    isPrivate: json["is_private"],
    isMarketer: json["is_marketer"],
    accountType: json["account_type"] == null ? null : json["account_type"],
    isAdmin: json["is_admin"],
    description: json["description"] == null ? null : json["description"],
    masterUserId: json["master_user_id"] == null ? null : json["master_user_id"],
    subUserCount: json["sub_user_count"],
    subUserRequestCount: json["sub_user_request_count"],
    rating: json["rating"].toDouble(),
    type: json["type"],
    // linkType: LinkType.fromJson(json["link_type"]),
    linkType: json["link_type"] == null ? null : LinkType.fromJson(json["link_type"]),
    isFollowed: json["is_followed"],
    deviceTokens: json["device_tokens"] == null ? null : List<DeviceToken>.from(json["device_tokens"].map((x) => DeviceToken.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "phone": phone,
    "authorized": authorized,
    "name": name,
    "image_url": imageUrl == null ? null : imageUrl,
    "shopping_country_id": shoppingCountryId,
    "email": email,
    "title": title,
    "access_central_market": accessCentralMarket,
    "access_fruits_market": accessFruitsMarket,
    "access_dates_market": accessDatesMarket,
    "access_equipments_market": accessEquipmentsMarket,
    "access_utilities_market": accessUtilitiesMarket,
    "access_services_market": accessServicesMarket,
    "access_occasions": accessOccasions,
    "is_private": isPrivate,
    "is_marketer": isMarketer,
    "account_type": accountType == null ? null : accountType,
    "is_admin": isAdmin,
    "description": description == null ? null : description,
    "master_user_id": masterUserId == null ? null : masterUserId,
    "sub_user_count": subUserCount,
    "sub_user_request_count": subUserRequestCount,
    "rating": rating,
    "type": type,
    //"link_type": linkType.toJson(),
    "link_type": linkType == null ? null : linkType.toJson(),

    "is_followed": isFollowed,
    "device_tokens": deviceTokens == null ? null : List<dynamic>.from(deviceTokens.map((x) => x.toJson())),
  };
}

class DeviceToken {
  DeviceToken({
    this.deviceType,
    this.appName,
    this.notificationToken,
  });

  String deviceType;
  String appName;
  String notificationToken;

  factory DeviceToken.fromJson(Map<String, dynamic> json) => DeviceToken(
    deviceType: json["device_type"],
    appName: json["app_name"],
    notificationToken: json["notification_token"],
  );

  Map<String, dynamic> toJson() => {
    "device_type": deviceType,
    "app_name": appName,
    "notification_token": notificationToken,
  };
}

class LinkType {
  LinkType({
    this.id,
    this.userId,
    this.linkedUserId,
    this.linkType,
    this.user1Status,
    this.user2Status,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int userId;
  int linkedUserId;
  dynamic linkType;
  String user1Status;
  String user2Status;
  DateTime createdAt;
  DateTime updatedAt;

  factory LinkType.fromJson(Map<String, dynamic> json) => LinkType(
    id: json["id"],
    userId: json["user_id"],
    linkedUserId: json["linked_user_id"],
    linkType: json["link_type"],
    user1Status: json["user_1_status"],
    user2Status: json["user_2_status"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "linked_user_id": linkedUserId,
    "link_type": linkType,
    "user_1_status": user1Status,
    "user_2_status": user2Status,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}

class Category {
  Category({
    this.id,
    this.icon,
    this.createdAt,
    this.updatedAt,
    this.parentId,
    this.order,
    this.key,
    this.color,
    this.isProduct,
    this.hasUnitType,
    this.hasRegisteredCompanies,
    this.hideInCreate,
    this.disabled,
    this.name,
  });

  int id;
  String icon;
  DateTime createdAt;
  DateTime updatedAt;
  int parentId;
  int order;
  String key;
  String color;
  bool isProduct;
  bool hasUnitType;
  bool hasRegisteredCompanies;
  bool hideInCreate;
  bool disabled;
  String name;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    icon: json["icon"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    parentId: json["parent_id"],
    order: json["order"],
    key: json["key"],
    color: json["color"],
    isProduct: json["is_product"],
    hasUnitType: json["has_unit_type"],
    hasRegisteredCompanies: json["has_registered_companies"],
    hideInCreate: json["hide_in_create"],
    disabled: json["disabled"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "icon": icon,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "parent_id": parentId,
    "order": order,
    "key": key,
    "color": color,
    "is_product": isProduct,
    "has_unit_type": hasUnitType,
    "has_registered_companies": hasRegisteredCompanies,
    "hide_in_create": hideInCreate,
    "disabled": disabled,
    "name": name,
  };
}

class Comment {
  Comment({
    this.id,
    this.comment,
    this.updatedAt,
    this.commenter,
  });

  int id;
  String comment;
  DateTime updatedAt;
  Marketer commenter;

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
    id: json["id"],
    comment: json["comment"],
    updatedAt: DateTime.parse(json["updated_at"]),
    commenter: Marketer.fromJson(json["commenter"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "comment": comment,
    "updated_at": updatedAt.toIso8601String(),
    "commenter": commenter.toJson(),
  };
}

class Image {
  Image({
    this.id,
    this.imageUrl,
  });

  int id;
  String imageUrl;

  factory Image.fromJson(Map<String, dynamic> json) => Image(
    id: json["id"],
    imageUrl: json["image_url"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "image_url": imageUrl,
  };
}

class Market {
  Market({
    this.id,
    this.name,
    this.order,
    this.lat,
    this.long,
    this.addressLine1,
  });

  int id;
  String name;
  dynamic order;
  double lat;
  double long;
  dynamic addressLine1;

  factory Market.fromJson(Map<String, dynamic> json) => Market(
    id: json["id"],
    name: json["name"],
    order: json["order"],
    lat: json["lat"],
    long: json["long"],
    addressLine1: json["address_line_1"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "order": order,
    "lat": lat,
    "long": long,
    "address_line_1": addressLine1,
  };
}

class Sound {
  Sound({
    this.id,
    this.soundUrl,
  });

  int id;
  String soundUrl;

  factory Sound.fromJson(Map<String, dynamic> json) => Sound(
    id: json["id"],
    soundUrl: json["sound_url"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "sound_url": soundUrl,
  };
}

class Pagination {
  Pagination({
    this.totalItems,
    this.totalPages,
    this.currentPage,
    this.perPage,
  });

  int totalItems;
  int totalPages;
  int currentPage;
  int perPage;

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
    totalItems: json["total_items"],
    totalPages: json["total_pages"],
    currentPage: json["current_page"],
    perPage: json["per_page"],
  );

  Map<String, dynamic> toJson() => {
    "total_items": totalItems,
    "total_pages": totalPages,
    "current_page": currentPage,
    "per_page": perPage,
  };
}

class User {
  User({
    this.id,
    this.phone,
    this.authorized,
    this.name,
    this.imageUrl,
    this.shoppingCountryId,
    this.email,
    this.title,
    this.accessCentralMarket,
    this.accessFruitsMarket,
    this.accessDatesMarket,
    this.accessEquipmentsMarket,
    this.accessUtilitiesMarket,
    this.accessServicesMarket,
    this.accessOccasions,
    this.isPrivate,
    this.isMarketer,
    this.accountType,
    this.isAdmin,
    this.description,
    this.masterUserId,
    this.subUserCount,
    this.subUserRequestCount,
    this.rating,
    this.type,
    this.linkType,
  });

  int id;
  String phone;
  bool authorized;
  String name;
  String imageUrl;
  int shoppingCountryId;
  String email;
  dynamic title;
  bool accessCentralMarket;
  bool accessFruitsMarket;
  bool accessDatesMarket;
  bool accessEquipmentsMarket;
  bool accessUtilitiesMarket;
  bool accessServicesMarket;
  bool accessOccasions;
  bool isPrivate;
  bool isMarketer;
  dynamic accountType;
  bool isAdmin;
  dynamic description;
  int masterUserId;
  int subUserCount;
  int subUserRequestCount;
  double rating;
  String type;
  LinkType linkType;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    phone: json["phone"],
    authorized: json["authorized"],
    name: json["name"],
    imageUrl: json["image_url"],
    shoppingCountryId: json["shopping_country_id"],
    email: json["email"],
    title: json["title"],
    accessCentralMarket: json["access_central_market"],
    accessFruitsMarket: json["access_fruits_market"],
    accessDatesMarket: json["access_dates_market"],
    accessEquipmentsMarket: json["access_equipments_market"],
    accessUtilitiesMarket: json["access_utilities_market"],
    accessServicesMarket: json["access_services_market"],
    accessOccasions: json["access_occasions"],
    isPrivate: json["is_private"],
    isMarketer: json["is_marketer"],
    accountType: json["account_type"],
    isAdmin: json["is_admin"],
    description: json["description"],
    masterUserId: json["master_user_id"],
    subUserCount: json["sub_user_count"],
    subUserRequestCount: json["sub_user_request_count"],
    rating: json["rating"],
    type: json["type"],
    linkType: json["link_type"] == null ? null : LinkType.fromJson(json["link_type"]),

  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "phone": phone,
    "authorized": authorized,
    "name": name,
    "image_url": imageUrl,
    "shopping_country_id": shoppingCountryId,
    "email": email,
    "title": title,
    "access_central_market": accessCentralMarket,
    "access_fruits_market": accessFruitsMarket,
    "access_dates_market": accessDatesMarket,
    "access_equipments_market": accessEquipmentsMarket,
    "access_utilities_market": accessUtilitiesMarket,
    "access_services_market": accessServicesMarket,
    "access_occasions": accessOccasions,
    "is_private": isPrivate,
    "is_marketer": isMarketer,
    "account_type": accountType,
    "is_admin": isAdmin,
    "description": description,
    "master_user_id": masterUserId,
    "sub_user_count": subUserCount,
    "sub_user_request_count": subUserRequestCount,
    "rating": rating,
    "type": type,
    "link_type": linkType == null ? null : linkType.toJson(),

  };
}