import 'package:app/cubits/card_state.dart';
import 'package:app/services/listApi.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

class CardCubit extends Cubit<CardState> {
  CardCubit({
    @required this.cardApi,
  })  :
        assert(cardApi != null),
        super(LoadingCard());

  final listApi cardApi;

  getItems() {
    emit(LoadingCard( ));
     cardApi.getItem().then((order) {
        emit(CardInitial(order));
      },
    ).catchError(
          (error) {
            print(error);
        emit(ErrorCard('an error occurred'));
      },
    );
  }
}