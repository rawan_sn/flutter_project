import 'package:app/models/listModel.dart';
import 'package:equatable/equatable.dart';

abstract class  CardState extends Equatable {
  const  CardState();
}

class CardInitial extends  CardState {

  CardInitial(this.messages);
  final listFromApi messages;

  @override
  List<Object> get props => [messages];
}

class LoadingCard extends  CardState {
  @override
  List<Object> get props => [];
}

class ErrorCard extends  CardState {
  ErrorCard(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}