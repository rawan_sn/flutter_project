import 'dart:convert';

import 'package:http/http.dart' as http;

enum RequestType { post, get,patch,delete,put}

class BaseApi {

  loadData(RequestType requestType, String url, {Map headers,Map body}) async {
    http.Response response;
    if (requestType == RequestType.post) {
      response = await _postRequest(url,headers:headers,body: body);
    }
    else if(requestType == RequestType.get) {
      response = await _getRequest(url, headers: headers);
    }
    else if(requestType == RequestType.delete) {
      response = await _deleteRequest(url, headers: headers);
    }
    else if(requestType == RequestType.put) {
      response = await _putRequest(url, headers: headers);
    }
    else
    {
      response = await _patchRequest(url, headers: headers,body: body);
    }

    print('Url: $url');
    print('StatusCode: ${response.statusCode}');
    print('Body: ${response.body}');

    if (response.statusCode == 200) {
      return response.body;
    }
  }

  _postRequest(String url,{Map<String, String> headers, Map body}) async {
    final response = await http.post(url, headers: headers, body: json.encode(body));
    return response;
  }

  _getRequest(String url, {Map<String, String> headers = const {}}) async {
    final response = await http.get(url, headers: headers);
    return response;
  }

  _patchRequest(String url,{Map<String, String> headers, Map body}) async {
    final response = await http.patch(url, headers: headers, body: json.encode(body));
    return response;
  }
  _deleteRequest(String url,{Map<String, String> headers}) async {
    final response = await http.delete(url, headers: headers);
    return response;
  }
  _putRequest(String url,{Map<String, String> headers}) async {
    final response = await http.put(url, headers: headers);
    return response;
  }
}