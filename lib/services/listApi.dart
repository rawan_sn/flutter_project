import 'dart:convert';
import 'package:app/models/listModel.dart';
import 'Base_api.dart';

class listApi extends BaseApi {
  Future<listFromApi> getItem() async {
    String url ='http://stg.agriunions.com:3000/users/api/v1/ads';
    Map<String, String> headers = {
      'accept': 'application/json',
    };
    var response = await loadData(RequestType.get, url, headers: headers);
    var jsonAsMap = json.decode(response);
    return listFromApi.fromJson(jsonAsMap);
  }
}