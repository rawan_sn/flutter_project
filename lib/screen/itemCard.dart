import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class cardItem extends StatelessWidget {
  String title;
  String name;
  String imageUrl;
  int views;
  String sold;
  String displayPrice;

  cardItem({
    @required this.title,
    @required this.name,
    @required this.imageUrl,
    @required this.views,
    @required this.sold,
    @required this.displayPrice,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width - 20,
        height: 95,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Color(0xFF492D72), width: 1),
          boxShadow: kElevationToShadow[4],
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(20), bottomLeft: Radius.circular(20)),
        ),
        child: Material(
            type: MaterialType.transparency,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: 60,
                            height: 70,
                            decoration: BoxDecoration(
                                boxShadow: kElevationToShadow[4],
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(20),
                                    bottomLeft: Radius.circular(20)),
                                image: DecorationImage(
                                    image: NetworkImage(imageUrl),
                                    fit: BoxFit.cover)),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(width: 35,),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(name,
                            style: TextStyle(
                                color: Color(0xFF492D72),
                                fontWeight: FontWeight.bold)),
                        Row(

                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                                width: 75,
                                child: Text(
                                  title,
                                  style: TextStyle(
                                      color: Color(0xFF492D72), fontSize: 14),
                                )),
                            SizedBox(
                              width: 20,height: 40,
                            ),
                            Container(width: 75, child: Text(displayPrice)),
                          ],
                        ),
                        SizedBox(width: 80,height: 3,),
                        IntrinsicHeight(
                            child: Container(
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Icon(
                                    Icons.remove_red_eye,
                                    size: 20,
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Container(
                                      width: 40,
                                      child: Text(views.toString())),
                                  VerticalDivider(
                                    thickness: 3,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Image.asset(
                                    "assets/icons/sold.png",
                                    height: 25,
                                    width: 20,
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Container(
                                      width: 40,
                                      child: Text(sold)),
                                ],
                              ),
                            )),
                      ],
                    ),
                  ],
                ),
              ],
            )));
  }
}
