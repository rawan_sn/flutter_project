import 'package:app/cubits/card_cubit.dart';
import 'package:app/cubits/card_state.dart';
import 'package:app/helper/AppColors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import 'itemCard.dart';

class OrderPage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<OrderPage> {
  CardCubit _cardCubit;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _cardCubit = BlocProvider.of<CardCubit>(context);
    _cardCubit.getItems();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF492D72),
          title: Text("List view"),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  width: size.width,
                  child: Column(
                    children: [
                      Container(
                        width: size.width,
                        child: BlocBuilder<CardCubit, CardState>(
                            buildWhen: (previousState, currentState) =>
                                currentState is CardState,
                            builder: (context, state) {
                              if (state is CardInitial) {
                                if (state.messages.data.items.isEmpty) {
                                  return Center(
                                      child: Text(
                                    'لا يوجد عناصر ',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 21,
                                      color: AppColors.black,
                                    ),
                                  ));
                                }
                                return SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      buildContainer(size, state),
                                    ],
                                  ),
                                );
                              } else if (state is ErrorCard) {
                                return Text(state.message);
                              }
                              return Column(
                                children: [
                                  SizedBox(
                                    height:
                                        MediaQuery.of(context).size.height / 3,
                                  ),
                                  Center(
                                    child: CircularProgressIndicator(
                                      valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              Color(0xFF492D72)),
                                    ),
                                  ),
                                ],
                              );
                            }),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  Widget buildContainer(Size size, CardInitial state) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
          width: size.width,
          height: size.height - 100,
          child: Center(
              child: Container(
            child: ListView.builder(
                itemCount: state.messages.data.items.length,
                itemBuilder: (BuildContext context, int index) {

                  return AnimationConfiguration.staggeredList(
                        position: index,
                        duration: const Duration(milliseconds: 800),
                        child: SlideAnimation(
                        horizontalOffset: MediaQuery.of(context).size.width,
                  child:Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: cardItem(
                      title: state.messages.data.items[index].title,
                      name: state.messages.data.items[index].category.name,
                      imageUrl: state.messages.data.items[index].imageUrl,
                      views: state.messages.data.items[index].views,
                      sold: state.messages.data.items[index].sold == null
                          ? "0"
                          : state.messages.data.items[index].sold,
                      displayPrice:
                          state.messages.data.items[index].displayPrice,
                    ),
                  )));
                }),
          ))),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
