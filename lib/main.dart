import 'package:app/screen/ItemsScreen.dart';
import 'package:app/services/listApi.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'cubits/card_cubit.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'flutter ',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
    ),
      home:
      BlocProvider(
        create: (context) => CardCubit(
          cardApi: listApi(),
        ),
        child: OrderPage(),
      ),
    );
  }
}